﻿//Declare a 2 class containing the same method. One method should be static. Now access the method from main class.

class A
{
    public void Meth()
    {
        Console.WriteLine("This is from class A.");
    }

    public static void Main(String[] args)
    {
        A a = new A();
        a.Meth();
        B.Meth();
    }
}


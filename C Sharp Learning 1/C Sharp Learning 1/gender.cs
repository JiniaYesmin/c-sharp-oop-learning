﻿
// Declare Enum. Convert a string to enum, int to enum, enum to string, enum to int
public enum Gender
{
    Male=1,
    Female=2,
    Not_Specified=3
}

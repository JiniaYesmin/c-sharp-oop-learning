﻿struct rectangle
{
    public int length { get; set; }
    public int width { get; set; }

    public int Area()
    {
        return (length * width);
    }
}
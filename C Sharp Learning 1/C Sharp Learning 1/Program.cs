﻿using C_Sharp_Learning_1;
using System.Collections.Immutable;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;


//Accessing the class elements by using its object.

var c = new Class1();
c.age = 22;
c.name = "Jinia";
c.meth();
Console.WriteLine();


//Accessing the structure elements by using its object.

var s=new rectangle();
s.length = 16;
s.width = 9;
Console.WriteLine("Area of the rectangle is: "+s.Area());
Console.WriteLine();

// class types are reference type

var array = new int[4] {1,2,3,4};
var array1 = array;
array1[2] = 10;
Console.WriteLine("Value of array: ");
foreach(var i in array)
{
    Console.Write(i+ " ");
}
Console.WriteLine();

// Primitive types are not reference type

int x = 10;
int y = x;
y++;
Console.WriteLine("Value of x is unchanged: "+x);

/*Bool array of size 15 with dummy data, printing until index 10 skipping index 5 with for,while,foreach, break,continue.*/

var BoolArray = new bool[15] {true,false,true,true,true,false, false,true,true,false, true,true,true,false,false};

Console.WriteLine("Bool Array with for loop: ");
int j;
for (j=0;j<15;j++)
{
    if (j == 5)
        continue;
    else if (j == 10)
    {
        Console.WriteLine(BoolArray[j]);
        break;
    }
    else
        Console.WriteLine(BoolArray[j]);
}
Console.WriteLine();
Console.WriteLine("Bool Array with while loop: ");
j = 0;
while (j < 15)
{
    if (j == 5) 
    {
        j++;
        continue;
    }
        
    else if (j == 10)
    {
        Console.WriteLine(BoolArray[j]);
        break;
    }
        
    else
        Console.WriteLine(BoolArray[j]);
    j++; 
}
Console.WriteLine();
Console.WriteLine("Bool Array with foreach loop: ");
j = 0;
foreach(bool n in BoolArray)
{
    if (j == 5)
    {
        j++;
        continue;
    }

    else if (j == 10)
    {
        Console.WriteLine(n);
        break;
    }   
    else
        Console.WriteLine(n);
    j++;
}


/*Character array of size 15 with dummy data, printing until index 10 skipping index 5 with for,while,foreach, break,continue.*/

var CharArray = new char[15] {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o'};
 
Console.WriteLine("Char Array with for loop: ");

for (j = 0; j < 15; j++)
{
    if (j == 5)
        continue;
    else if (j == 10)
    {
        Console.WriteLine(CharArray[j]);
        break;
    }  
    else
        Console.WriteLine(CharArray[j]);
}
Console.WriteLine();
Console.WriteLine("Char Array with while loop: ");
j = 0;
while (j < 15)
{
    if (j == 5)
    {
        j++;
        continue;
    }

    else if (j == 10)
    {
        Console.WriteLine(CharArray[j]);
        break;
    }
        
    else
        Console.WriteLine(CharArray[j]);
    j++;
}
Console.WriteLine();
Console.WriteLine("Char Array with foreach loop: ");
j = 0;
foreach (char n in CharArray)
{
    if (j == 5)
    {
        j++;
        continue;
    }

    else if (j == 10)
    {
        Console.WriteLine(n);
        break;
    }
        
    else
        Console.WriteLine(n);
    j++;
}
/*String array of size 15 with dummy data, printing until index 10 skipping index 5 with for,while,foreach, break,continue.*/

var StringArray = new string[15] {"Ant","Bat","Cat","Dog","Elephant","Frog","Goat","Hen","Ibis","Jaguar","Kangaroo", "Lamb","Monkey","Narwhal","Ostrich"};
Console.WriteLine("String Array with for loop: ");

for (j = 0; j < 15; j++)
{
    if (j == 5)
        continue;
    else if (j == 10)
    {
        Console.WriteLine(StringArray[j]);
        break;
    }
       
    else
        Console.WriteLine(StringArray[j]);
}
Console.WriteLine();
Console.WriteLine("String Array with while loop: ");
j = 0;
while (j < 15)
{
    if (j == 5)
    {
        j++;
        continue;
    }

    else if (j == 10)
    {
        Console.WriteLine(StringArray[j]);
        break;
    }
        
    else
        Console.WriteLine(StringArray[j]);
    j++;
}
Console.WriteLine();
Console.WriteLine("String Array with foreach loop: ");
j = 0;
foreach (string n in StringArray)
{
    if (j == 5)
    {
        j++;
        continue;
    }

    else if (j == 10)
    {
        Console.WriteLine(n);
        break;
    }
    else
        Console.WriteLine(n);
    j++;
}


/*Integer array of size 15 with dummy data, printing until index 10 skipping index 5 with for,while,foreach, break,continue.*/

var IntArray = new int[15] { 1, 2, 3, 4,5,6,7,8,9,10,11,12,13,14,15 };
Console.WriteLine("Integer Array with for loop: ");

for (j = 0; j < 15; j++)
{
    if (j == 5)
        continue;
    else if (j == 10)
    {
        Console.WriteLine(IntArray[j]);
        break;
    }
        
    else
        Console.WriteLine(IntArray[j]);
}
Console.WriteLine();
Console.WriteLine("Integer Array with while loop: ");
j = 0;
while (j < 15)
{
    if (j == 5)
    {
        j++;
        continue;
    }

    else if (j == 10)
    {
        Console.WriteLine(IntArray[j]);
        break;
    }
        
    else
        Console.WriteLine(IntArray[j]);
    j++;
}
Console.WriteLine();
Console.WriteLine("Integer Array with foreach loop: ");
j = 0;
foreach (int n in IntArray)
{
    if (j == 5)
    {
        j++;
        continue;
    }

    else if (j == 10)
    {
        Console.WriteLine(n);
        break;
    }
        
    else
        Console.WriteLine(n);
    j++;
}


/*Long array of size 15 with dummy data, printing until index 10 skipping index 5 with for,while,foreach, break,continue.*/

var LongArray = new long[15] { 18, 82, 93, 54, 57, 76, 17, 38,79, 150, 171, 132, 163, 144, 615 };
Console.WriteLine("Long Array with for loop: ");

for (j = 0; j < 15; j++)
{
    if (j == 5)
        continue;
    else if (j == 10)
    {
        Console.WriteLine(LongArray[j]);
        break;
    }

    else
        Console.WriteLine(LongArray[j]);
}
Console.WriteLine();
Console.WriteLine("Long Array with while loop: ");
j = 0;
while (j < 15)
{
    if (j == 5)
    {
        j++;
        continue;
    }

    else if (j == 10)
    {
        Console.WriteLine(LongArray[j]);
        break;
    }

    else
        Console.WriteLine(LongArray[j]);
    j++;
}
Console.WriteLine();
Console.WriteLine("Long Array with foreach loop: ");
j = 0;
foreach (long n in LongArray)
{
    if (j == 5)
    {
        j++;
        continue;
    }

    else if (j == 10)
    {
        Console.WriteLine(n);
        break;
    }

    else
        Console.WriteLine(n);
    j++;
}

/*Double array of size 15 with dummy data, printing until index 10 skipping index 5 with for,while,foreach, break,continue.*/

var DoubleArray = new double[15] { 18.32, 82.36, 93.98, 54.98, 57.74, 76.15, 17.42, 38.32, 79.91, 150.14, 171.78, 132.31, 163.45, 144.22, 615.47 };
Console.WriteLine("Double Array with for loop: ");

for (j = 0; j < 15; j++)
{
    if (j == 5)
        continue;
    else if (j == 10)
    {
        Console.WriteLine(LongArray[j]);
        break;
    }

    else
        Console.WriteLine(LongArray[j]);
}
Console.WriteLine();
Console.WriteLine("Double Array with while loop: ");
j = 0;
while (j < 15)
{
    if (j == 5)
    {
        j++;
        continue;
    }

    else if (j == 10)
    {
        Console.WriteLine(DoubleArray[j]);
        break;
    }

    else
        Console.WriteLine(DoubleArray[j]);
    j++;
}
Console.WriteLine();
Console.WriteLine("Double Array with foreach loop: ");
j = 0;
foreach (double n in DoubleArray)
{
    if (j == 5)
    {
        j++;
        continue;
    }

    else if (j == 10)
    {
        Console.WriteLine(n);
        break;
    }

    else
        Console.WriteLine(n);
    j++;
}
Console.WriteLine();


// String to Enum, Enum to string
var gen = "Male";
var gender = (Gender) Enum.Parse(typeof(Gender), gen);
Console.WriteLine(gender.ToString());
Console.WriteLine();

//Integer to Enum, Enum to Integer
int g = 3;
Console.WriteLine((Gender)g);
Console.WriteLine();
var Gen = Gender.Female;
Console.WriteLine((int)Gen);

// Declaring a List of int and implementing all list manipulation methods on it.

List<int> IntList = new List<int>();
IntList.Add(10);
IntList.Add(11);
int[] Ints = { 20, 32, 67, 91, 45, 23, 24,57,39 };
IntList.AddRange(Ints);
IList<int> roIntList = IntList.AsReadOnly();

Console.WriteLine("\nElements in the read-only IList:");
foreach (int intList in roIntList)
{
    Console.WriteLine(intList);
}
IntList.Sort();
Console.WriteLine("\nElements in the List in sorted order:");
foreach (int intList in IntList)
{
    Console.WriteLine(intList);
}
int index = IntList.BinarySearch(45);
Console.WriteLine("\nIndex of 45 found by Binary search: "+index);

Console.WriteLine("\nThe list contains 30: "+IntList.Contains(30));
IntList.CopyTo(3, Ints, 1, 8);
Console.WriteLine("\nCopied array from List: ");
foreach (int ints in Ints)
{
    Console.WriteLine(ints);
}

IntList.EnsureCapacity(1000);

List<Product> List1 = new List<Product>();
List1.Add(new Product() { PName = "Soap", PId = 1234 });
List1.Add(new Product() { PName = "Shampoo", PId = 1334 });
List1.Add(new Product() { PName = "Face Wash", PId = 1434 });
List1.Add(new Product() { PName = "Lotion", PId = 1444 });
List1.Add(new Product() { PName = "Toner", PId = 1534 });
List1.Add(new Product() { PName = "Sunscreen", PId = 1634 });

Console.WriteLine("\nExists: Product with Id=1444: "+
            List1.Exists(x => x.PId == 1444));
IntList.Remove(32);
Console.WriteLine("\nIntList after removing 32: ");
foreach (int intList in IntList)
{
    Console.WriteLine(intList);
}
IntList.Reverse();
Console.WriteLine("\nIntList after reverse: ");
foreach (int intList in IntList)
{
    Console.WriteLine(intList);
}

IntList.Clear();
Console.WriteLine("IntList after Clearing: ");
foreach (int intList in IntList)
{
    Console.WriteLine(intList);
}

// Declaring a List of double and implementing all list manipulation methods on it.

List<double> DoubleList = new List<double>();
DoubleList.Add(10.56);
DoubleList.Add(11);
double[] Doubles = { 20.93, 32.26, 67.34, 91.54, 45.84, 23.81, 24.56, 57.36, 39.31 };
DoubleList.AddRange(Doubles);
IList<double> roDoubleList = DoubleList.AsReadOnly();

Console.WriteLine("\nElements in the read-only IList:");
foreach (double doubleList in roDoubleList)
{
    Console.WriteLine(doubleList) ;
}
DoubleList.Sort();
Console.WriteLine("\nElements in the List in sorted order:");
foreach (double doubleList in DoubleList)
{
    Console.WriteLine(doubleList);
}
 index = DoubleList.BinarySearch(91.54);
Console.WriteLine("\nIndex of 45 found by Binary search: " + index);

Console.WriteLine("\nThe list contains 23.81: " + DoubleList.Contains(30));
DoubleList.CopyTo(3, Doubles, 1, 8);
Console.WriteLine("\nCopied array from List: ");
foreach (double doubles in Doubles)
{
    Console.WriteLine(doubles);
}

DoubleList.EnsureCapacity(600);
DoubleList.Remove(20.93);
Console.WriteLine("\nDoubleList after removing 20.93: ");
foreach (double doubleList in DoubleList)
{
    Console.WriteLine(doubleList);
}
DoubleList.Reverse();
Console.WriteLine("\nDoubleList after reverse: ");
foreach (double doubleList in DoubleList)
{
    Console.WriteLine(doubleList);
}

DoubleList.Clear();
Console.WriteLine("DoubleList after Clearing: ");
foreach (double doubleList in DoubleList)
{
    Console.WriteLine(doubleList);
}

// Declaring a List of String and implementing all list manipulation methods on it.

List<string> StringList = new List<string>();
StringList.Add("Jinia");
StringList.Add("Araf");
string[] Strings = { "Tarikul", "Jamil", "Tazeen", "Islam", "Katy" };
StringList.AddRange(Strings);
IList<string> roStringList = StringList.AsReadOnly();

Console.WriteLine("\nElements in the read-only IList:");
foreach (string stringList in roStringList)
{
    Console.WriteLine(stringList);
}
StringList.Sort();
Console.WriteLine("\nElements in the List in sorted order:");
foreach (string stringList in StringList)
{
    Console.WriteLine(stringList);
}
index = StringList.BinarySearch("Tarikul");
Console.WriteLine("\nIndex of \"Tarikul\" found by Binary search: " + index);

Console.WriteLine("\nThe list contains \"Araf\": " + StringList.Contains("Araf"));
StringList.CopyTo(3, Strings, 1, 3);
Console.WriteLine("\nCopied array from List: ");
foreach (string strings in Strings)
{
    Console.WriteLine(strings);
}

StringList.EnsureCapacity(600);
StringList.Remove("Katy");
Console.WriteLine("\nStringList after removing \"Katy\": ");
foreach (string stringList in StringList)
{
    Console.WriteLine(stringList);
}
StringList.Reverse();
Console.WriteLine("\nStringList after reverse: ");
foreach (string stringList in StringList)
{
    Console.WriteLine(stringList);
}

StringList.Clear();
Console.WriteLine("StringList after Clearing: ");
foreach (string stringList in StringList)
{
    Console.WriteLine(stringList);
}


// Declare an array of int. Find out all array manipulation methods and implement them.
var Array1 = new int[10] {3,8,12,98,90,189,100,46,84,10};
Array.Reverse(Array1);
Console.WriteLine("\nReverse array: ");
foreach(int arr in Array1)
{
    Console.WriteLine(arr);
}

var Array2 = new int[15];
Array.Copy(Array1, Array2, 9);
Console.WriteLine("\nArray2 copied fom Array1: ");
foreach (int arr in Array2)
{
    Console.WriteLine(arr);
}

Array.Sort(Array1);

Console.WriteLine("\nSorted array: ");
foreach (int arr in Array1)
{
    Console.WriteLine(arr);
}

Console.WriteLine( "Index of 90 in Array1: "+Array.IndexOf(Array1, 90) );
Console.WriteLine( "\nIndex of 46 found by Binary Search: "+Array.BinarySearch(Array1, 46) );

Array.Clear(Array1, 2, 6);
Console.WriteLine("\nArray1 cleared from index 2-6: ");
foreach (int arr in Array1)
{
    Console.WriteLine(arr);
}

// Declare an array of double. Find out all array manipulation methods and implement them.
var Array3 = new double[10] { 3.1, 8.5, 12.2, 98.8, 90.6, 189.21, 100.254, 4.66, 84.35, 10.5 };
Array.Reverse(Array3);
Console.WriteLine("\nReverse array: ");
foreach (double arr in Array3)
{
    Console.WriteLine(arr);
}

var Array4 = new double[15];
Array.Copy(Array3, Array4, 7);
Console.WriteLine("\nArray4 copied fom Array3: ");
foreach (double arr in Array4)
{
    Console.WriteLine(arr);
}

Array.Sort(Array3);

Console.WriteLine("\nSorted array: ");
foreach (double arr in Array3)
{
    Console.WriteLine(arr);
}

Console.WriteLine("Index of 12.2 in Array3: " + Array.IndexOf(Array3, 12.2));
Console.WriteLine("\nIndex of 10.5 found by Binary Search: " + Array.BinarySearch(Array3, 10.5));

Array.Clear(Array3, 3, 5);
Console.WriteLine("\nArray3 cleared from index 3-5: ");
foreach (double arr in Array3)
{
    Console.WriteLine(arr);
}

// Declare an array of String. Find out all array manipulation methods and implement them.
var Array5 = new String[10] { "Flour", "Milk", "Butter", "Curd", "Lemon", "Egg", "Bread","Sugar", "Salt" , "Cream"};
Array.Reverse(Array5);
Console.WriteLine("\nReverse array: ");
foreach ( string arr in Array5)
{
    Console.WriteLine(arr);
}

var Array6 = new string[15];
Array.Copy(Array5, Array6,8);
Console.WriteLine("\nArray6 copied fom Array5: ");
foreach (string arr in Array6)
{
    Console.WriteLine(arr);
}

Array.Sort(Array5);

Console.WriteLine("\nSorted array: ");
foreach (string arr in Array5)
{
    Console.WriteLine(arr);
}

Array.Clear(Array3, 3, 5);
Console.WriteLine("\nArray4 cleared from index 3-5: ");
foreach (string arr in Array5)
{
    Console.WriteLine(arr);
}

// Datetime, timespan

var dateTime = new DateTime(2022, 7, 5);
var now = DateTime.Now;
var today = DateTime.Today;
Console.WriteLine(dateTime);
Console.WriteLine(now);
Console.WriteLine(today);
var lastWeek = now.AddDays(-7);
var nextMonth = now.AddMonths(1);
Console.WriteLine(lastWeek);
Console.WriteLine(nextMonth);

Console.WriteLine(now.ToLongDateString());
Console.WriteLine(now.ToShortDateString());
Console.WriteLine(now.ToLongTimeString());
Console.WriteLine(now.ToShortTimeString());
Console.WriteLine(now.ToString("yyyy-MM-dd  HH:mm:ss"));

var timeSpan = new TimeSpan(1, 3, 6);
var timeSpan1 = TimeSpan.FromHours(2);
var start = DateTime.Now;
var end = DateTime.Now.AddMinutes(5);
var duration = end - start;
Console.WriteLine(duration);
Console.WriteLine(timeSpan.Minutes);
Console.WriteLine(timeSpan.TotalMinutes);
Console.WriteLine("Add time: "+ timeSpan.Add(TimeSpan.FromMinutes(4)));
Console.WriteLine("Subtract time: " + timeSpan.Subtract(TimeSpan.FromMinutes(2)));

Console.WriteLine("Parse: "+ TimeSpan.Parse("12:21:08"));

// Strings
string String = " I am Jinia ";
string String1 = "Jinia";
string[] StrArr = String.Split();
foreach(string Str in StrArr)
{
    Console.WriteLine(Str);
}
Console.WriteLine(String.Compare(StrArr[2], String1));
Console.WriteLine(String.Replace("Jinia", "Tonni"));
Console.WriteLine(String.Contains("am"));
string Stri = String.Remove(2,3);
Console.WriteLine(Stri);
Console.WriteLine("Trimmed String: " +String.Trim() );
Console.WriteLine("IndexOf J in String: "+ String.IndexOf("J"));
Console.WriteLine("String in uppercase: "+String.ToUpper());
Console.WriteLine("String in lowercase: " + String.ToLower());
Console.WriteLine("Last index of i in String: " + String.LastIndexOf("i"));
string String3 = string.Join( ",",Array5);
Console.WriteLine("String join: "+ String3);

StringBuilder sb = new StringBuilder(String);
Console.WriteLine("String before modification: "+sb);
sb.Replace("I", "She");
sb.Replace("am", "is");
Console.WriteLine("String after modification: " + sb);

sb.Append("Yesmin");
Console.WriteLine("String after appending: "+sb);
sb.Remove(5, 3);
Console.WriteLine("String after removing: " + sb);
sb.Insert(5,"Was ");
Console.WriteLine("String after inserting: " + sb);



